#!/bin/bash

# define here, which version should be installed
# goto https://www.python.org/downloads/ and select the versions you like to install
PYTHON_VERSIONS=(
#  Version->Alias - Alias Name (the python version will be install on the system with the alias name python3.8, python 3.9 ...)
#    "3.7.17 - 3.7"
    "3.8.18 - 3.8"
    "3.9.18 - 3.9"
    "3.10.12 - 3.10"
    "3.11.8 - 3.11"
#    "3.12.2 - 3.12" -> is the default in docker image
)

# Install Python Versions
for version in "${PYTHON_VERSIONS[@]}"; 
do 
    readarray -d " - " -t SPLIT <<< "${version}"
    PYTHON_VERSON=${SPLIT[0]}
    PYTHON_ALIAS=${SPLIT[2]}

    echo "DOWNLOAD Python Version ${PYTHON_VERSON}"
    wget https://www.python.org/ftp/python/${PYTHON_VERSON}/Python-${PYTHON_VERSON}.tgz

    # extract the file
    tar -xzf Python-${PYTHON_VERSON}.tgz
    # Remove tar
    rm -f Python-${PYTHON_VERSON}.tgz
    # select the directory
    cd Python-${PYTHON_VERSON}
    
    # install the version as python alternative 
    echo "run ./configure --enable-optimizations"
    ./configure --enable-optimizations
    # make
    # make test
    
    echo "run make altinstall"
    make altinstall
    
    cd ..
    rm -rf Python-${PYTHON_VERSON}

    echo "${PYTHON_VERSON} installed";
    
    
    # upgrade pip
    echo "Upgrade PIP for Python Version ${PYTHON_VERSON}"
    python${PYTHON_ALIAS} -m pip install --upgrade pip
    python${PYTHON_ALIAS} -m pip --version
done