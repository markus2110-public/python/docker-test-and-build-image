FROM python:3.12-alpine

# --no-cache --virtual .build-deps

RUN apk add  \
        bash \
		bluez-dev \
		bzip2-dev \
		coreutils \
		dpkg-dev dpkg \
		expat-dev \
		findutils \
		gcc \
		gdbm-dev \
		libc-dev \
		libffi-dev \
		libnsl-dev \
		libtirpc-dev \
		linux-headers \
		make \
		ncurses-dev \
		openssl-dev \
		pax-utils \
		readline-dev \
		sqlite-dev \
		tcl-dev \
		tk \
		tk-dev \
		util-linux-dev \
		xz-dev \
		zlib-dev

RUN bash

WORKDIR /usr/src/python

# Add the Python Setup script
ADD setup.sh .
RUN ./setup.sh

# Remove build debs
#RUN apk del --no-network .build-deps \
#    && rm -rf /var/cache/apk/*


CMD ["python"]